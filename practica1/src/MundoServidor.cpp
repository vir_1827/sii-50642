// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>

#include <fstream>
#include "signal.h"
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <error.h>

#define TAM_BUFF 55

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d);



void  matar_proceso (int signum){

	if(signum!=10){
		printf("El proceso finalizado por la recepcion de una señal \n ");
		printf("La señal que ha finalizado el proceso es:%d \n ",signum);
		exit (1);
		}
	else{
		printf("El proceso ha finalizado correctamente");
		exit(0);
	}

}

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	char buf[]="Finalizado";
	write(fd, buf,sizeof(buf));
	close(fd);
	comunicacion.Close();
	conexion.Close();
	//close(fd2);
	//close(fd3);
	
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.radio-=0.0005;
	if(esfera.radio<0.2f) esfera.radio=0.2f;
	int i;
	char buff[TAM_BUFF];
	
	
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=3+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=3+2*rand()/(float)RAND_MAX;
		puntos2++;

		sprintf(buff,"El jugador 2 marca 1 punto, lleva un total de %d puntos\n",puntos2);
		write(fd,buff,sizeof(buff));
	}
		
		
	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-3-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-3-2*rand()/(float)RAND_MAX;
		puntos1++;

		sprintf(buff,"El jugador 1 marca 1 punto, lleva un total de %d puntos\n",puntos1);
		write(fd,buff,sizeof(buff));
	}
	
	
	//ESCRITURA EN LA TUBERÍA SERVIDOR-CLIENTE
	
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	//write(fd2,cad,sizeof(cad));

	//Sockets
	
	
	comunicacion.Send(cad,200);
	
	//Finalizar por desconexion cliente
	/*char fin[2];
	comunicacion.Receive(fin,2);
	if(fin=="1")
		kill(getpid(),SIGUSR1);*/
		
	
	
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	//case 'a':jugador1.velocidad.x=-4;break;
    //case 'd':jugador1.velocidad.x=4;break;
	//case 's':jugador1.velocidad.y= esfera.velocidad.y-1;break;
	//case 'w':jugador1.velocidad.y=esfera.velocidad.y+1;break;
	//case 'l':jugador2.velocidad.y=-4;break;
	//case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//Apertura tuberia SERVIDOR-CLIENTE
	//fd2=open("/tmp/mififo2",O_WRONLY);
	
	//cliente-servidor
	
	//fd3=open("/tmp/mififo3",O_RDONLY);
	
	
	//Apertura de la tuberia fifo
	fd=open("/tmp/mififo1",O_WRONLY);
	
		//if( fd3==-1 ||fd2==-1 || fd2==-1)
			//perror("Error de apertura");

	//hilo
	pthread_create(&th1, NULL, hilo_comandos, this);

	//Ejercicio propuesto señales
	struct sigaction act;


	act.sa_handler = matar_proceso;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaction(SIGTERM, &act, NULL);
	sigaction(SIGINT, &act, NULL);
	sigaction(SIGPIPE, &act, NULL);
	sigaction(SIGUSR1, &act, NULL);
	
	//sockets
	//infconfig
	char micadena[TAM_BUFF];
	conexion.InitServer((char*)"127.0.0.1",3550);
		
	comunicacion=conexion.Accept();
	comunicacion.Receive(micadena,TAM_BUFF);
	printf("Nombre cliente : %s \n",micadena);
	

}


void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}
	

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[TAM_BUFF];
           // read(fd3, cad, sizeof(cad));
           comunicacion.Receive(cad,TAM_BUFF);
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
            if(key=='t' && (puntos1==5 || puntos2==5))exit(0);
}
}
