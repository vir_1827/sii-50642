#include <iostream>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"

#define DIF_CENTRO 1.0f //indica la diferencia de la posicion y del plano con el centro
int main(){
	int file;
	DatosMemCompartida* pMemC;
	char* org;

	file=open("/tmp/datosComp.txt",O_RDWR);

	

	org=(char*)mmap(NULL,sizeof(*(pMemC)),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	close(file);
	
	pMemC=(DatosMemCompartida*)org;


	while(1){
		float posicion_y;
		posicion_y=(pMemC->raqueta1.y2+pMemC->raqueta1.y1)/2;
		//std::cout<<pMemC->esfera.centro.y<<" Soy el Bot"<<std::endl;
		if(posicion_y<pMemC->esfera.centro.y)
			pMemC->accion=1;
		else if(posicion_y>pMemC->esfera.centro.y)
			pMemC->accion=-1;
		//if(pMemC->raqueta1.y2-DIF_CENTRO-pMemC->esfera.centro.y<0.001f && pMemC->raqueta1.y2-DIF_CENTRO-pMemC->esfera.centro.y>-0.001f)
		else
			pMemC->accion=0;
			
		usleep(25000);
	}
	munmap(org,sizeof(*(pMemC)));

}

