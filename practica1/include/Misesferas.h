// Misesferas.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#ifndef MISESFERAS_H
#define MISESFERAS_H
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <vector>
#include "Esfera.h"


#pragma once
 
class Misesferas{
public:
	std::vector<Esfera*> esferas;



	Misesferas(){
	//Crea primero una esfera
	if (esferas.size()==0)
		esferas.push_back(new Esfera());
	}
	~Misesferas(){esferas.clear();};
	void Dibuja(){	for(int i=0;i<esferas.size();i++)
		esferas[i]->Dibuja();}



	void Mueve (float m){
	for (int i=0;i<esferas.size();i++)
		esferas[i]->Mueve(m);};
	void contartiempo(){for(int i=0;i<esferas.size();i++){
		esferas[i]->contartiempo();
		esferas[i]->Cambiartamano();};

	void Agregar(){
		for(int i=0;i<esferas.size();i++){
			int tiempo=int (esferas[i]->Missegundos());
			if (tiempo%29==0)
				esferas.push_back(new Esfera());}
	};



};
#endif
